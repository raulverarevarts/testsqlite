﻿using System.Data;
using System.Collections;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;

namespace ExampleProject
{
    public class SQLiteExample : MonoBehaviour
    {
        public Text _logText;

        private string dbPath;

        private void Start()
        {
            dbPath = "URI=file:" + Application.persistentDataPath + "/exampleDatabase.db";
            CreateSchema();
            InsertScore("GG Meade", 3701);
            InsertScore("US Grant", 4242);
            InsertScore("GB McClellan", 107);
            GetHighScores(10);
			
			StartCoroutine(Operation());
        }

        public void CreateSchema()
        {
            using (var conn = new SqliteConnection(dbPath))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "CREATE TABLE IF NOT EXISTS 'high_score' ( " +
                                      "  'id' INTEGER PRIMARY KEY, " +
                                      "  'name' TEXT NOT NULL, " +
                                      "  'score' INTEGER NOT NULL" +
                                      ");";

                    var result = cmd.ExecuteNonQuery();
                    Log("create schema: " + result);
                }
            }
        }

        public void InsertScore(string highScoreName, int score)
        {
            using (var conn = new SqliteConnection(dbPath))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO high_score (name, score) " +
                                      "VALUES (@Name, @Score);";

                    cmd.Parameters.Add(new SqliteParameter
                    {
                        ParameterName = "Name",
                        Value = highScoreName
                    });

                    cmd.Parameters.Add(new SqliteParameter
                    {
                        ParameterName = "Score",
                        Value = score
                    });

                    var result = cmd.ExecuteNonQuery();
                    Log("insert score: " + result);
                }
            }
        }

        public void GetHighScores(int limit)
        {
            using (var conn = new SqliteConnection(dbPath))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM high_score ORDER BY id DESC LIMIT @Count;";

                    cmd.Parameters.Add(new SqliteParameter
                    {
                        ParameterName = "Count",
                        Value = limit
                    });

                    Log("scores (begin)");
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var id = reader.GetInt32(0);
                        var highScoreName = reader.GetString(1);
                        var score = reader.GetInt32(2);
                        var text = string.Format("{0}: {1} [#{2}]", highScoreName, score, id);
                        Log(text);
                    }
                    Log("scores (end)");
                }
            }
        }


        IEnumerator Operation()
        {
            while (true)
            {
                using (var conn = new SqliteConnection(dbPath))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "INSERT INTO high_score (name, score) " +
                                          "VALUES (@Name, @Score);";

                        var s = new System.Text.StringBuilder("");
                        for (int i = 0; i < 10; ++i)
                        {
                            s.Append((char)Random.Range(64, 128));
                        }


                        cmd.Parameters.Add(new SqliteParameter
                        {
                            ParameterName = "Name",
                            Value = s.ToString()
                        });

                        cmd.Parameters.Add(new SqliteParameter
                        {
                            ParameterName = "Score",
                            Value = Random.Range(0, 10000)
                        });

                        var result = cmd.ExecuteNonQuery();
                        Log("insert score: " + result);
                    }
                }

                yield return new WaitForSeconds(0.5f);
				
                GetHighScores(1);
				yield return new WaitForSeconds(0.5f);
            }
        }

        void Log(string str)
        {
			if (_logText != null)
            	_logText.text += str + "\n";
        }
    }

}